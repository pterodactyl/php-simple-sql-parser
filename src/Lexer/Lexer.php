<?php

namespace PHPSimpleSqlParser\Lexer;

use PHPSimpleSqlParser\IO\Reader\Reader;
use function PHPSimpleSqlParser\isDigit;
use function PHPSimpleSqlParser\isLetter;
use function PHPSimpleSqlParser\isWhitespace;

class Lexer
{
    private Reader $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * Scan next token
     */
    public function scan(): Token
    {
        $char = $this->read();

        if (isWhitespace($char)) {
            $this->unread();

            return $this->scanWhitespace();
        }

        if (isLetter($char)) {
            $this->unread();

            return $this->scanIdentifier();
        }

        return match ($char) {
            '*'     => new Asterisk($char),
            ','     => new Comma($char),
            null    => new Eof(''),
            default => new Illegal($char),
        };
    }

    private function read(): ?string
    {
        return $this->reader->read();
    }

    private function unread(): void
    {
        $this->reader->unread();
    }

    private function scanWhitespace(): Token
    {
        $buf = $this->read();

        while (true) {
            $ch = $this->read();

            if ($ch == null) {
                break;
            }

            if (!isWhitespace($ch)) {
                $this->unread();
                break;
            }

            $buf .= $ch;
        }

        return new Whitespace($buf);
    }

    private function scanIdentifier(): Token
    {
        $buf = $this->read();

        while (true) {
            $ch = $this->read();

            if ($ch === null) {
                break;
            }

            if (!isLetter($ch) && !isDigit($ch) && $ch != '_') {
                $this->unread();
                break;
            }

            $buf .= $ch;
        }

        return match ($buf) {
            'FROM' => new From($buf),
            'SELECT' => new Select($buf),

            default => new Identifier($buf),
        };
    }
}
