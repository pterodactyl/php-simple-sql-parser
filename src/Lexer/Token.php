<?php

namespace PHPSimpleSqlParser\Lexer;

abstract class Token
{
    public function __construct(
        public readonly string $literal,
    )
    {
    }
}
