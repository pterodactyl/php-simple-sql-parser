<?php

namespace PHPSimpleSqlParser;

class SelectStatement
{
    /** @var string[] */
    public array $fields;

    public string $tableName;
}
