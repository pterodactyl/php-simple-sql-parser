<?php

namespace PHPSimpleSqlParser\IO\Reader;

class Reader
{
    private int $length;
    private int $position = 0;

    public function __construct(
        private string $query,
    )
    {
        $this->length = mb_strlen($this->query);
    }

    public function read(): ?string
    {
        if (!$this->eof()) {
            // read current char and step forward
            return $this->query[$this->position++];
        }

        return null;
    }

    public function unread(): void
    {
        if ($this->position > 0) {
            $this->position--;
        }
    }

    private function eof(): bool
    {
        return $this->position === $this->length;
    }
}
