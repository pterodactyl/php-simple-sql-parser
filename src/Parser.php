<?php

namespace PHPSimpleSqlParser;

use Exception;
use PHPSimpleSqlParser\Lexer\Asterisk;
use PHPSimpleSqlParser\Lexer\Comma;
use PHPSimpleSqlParser\Lexer\From;
use PHPSimpleSqlParser\Lexer\Identifier;
use PHPSimpleSqlParser\Lexer\Select;
use PHPSimpleSqlParser\Lexer\Token;
use PHPSimpleSqlParser\Lexer\Lexer;
use PHPSimpleSqlParser\Lexer\Whitespace;

class Parser
{
    private Token $latestToken;

    private int $bufferSize = 0;

    public function __construct(
        private readonly Lexer $lexer,
    )
    {
    }

    /**
     * Parse the query and return select statement.
     *
     * @throws \Exception
     */
    public function parse(): SelectStatement
    {
        $statement = new SelectStatement();

        $token = $this->scanIgnoreWhitespace();

        // make sure there's a SELECT token
        if (!$token instanceof Select) {
            throw new Exception("found $token->literal, expected SELECT");
        }

        // next, parse a comma-delimited list of fields
        while (true) {
            $token = $this->scanIgnoreWhitespace();

            if (!$token instanceof Identifier && !$token instanceof Asterisk) {
                throw new Exception("found $token->literal, expected field");
            }

            $statement->fields[] = $token->literal;

            // read next token
            $token = $this->scanIgnoreWhitespace();

            // if next token is not a comma, break the loop
            if (!$token instanceof Comma) {
                $this->unscan();
                break;
            }
        }

        // next we should see the FROM keyword
        $token = $this->scanIgnoreWhitespace();

        if (!$token instanceof From) {
            throw new Exception("found $token->literal, expected FROM");
        }

        $token = $this->scanIgnoreWhitespace();

        if (!$token instanceof Identifier) {
            throw new Exception("found $token->literal, expected table name");
        }

        $statement->tableName = $token->literal;

        return $statement;
    }

    private function scan(): Token
    {
        // if we have a token on the buffer, then return it
        if ($this->bufferSize > 0) {
            $this->bufferSize = 0;
            return $this->latestToken;
        }

        return $this->latestToken = $this->lexer->scan();
    }

    private function unscan(): void
    {
        $this->bufferSize = 1;
    }

    private function scanIgnoreWhitespace(): Token
    {
        $token = $this->scan();

        if ($token instanceof Whitespace) {
            $token = $this->scan();
        }

        return $token;
    }
}
