<?php

namespace PHPSimpleSqlParser;

if (!function_exists('isWhitespace')) {
    function isWhitespace(?string $char): bool
    {
        return $char == ' ' || $char == '\t' || $char == '\n';
    }
}

if (!function_exists('isLetter')) {
    function isLetter(?string $char): bool
    {
        return ($char >= 'a' && $char <= 'z') || ($char >= 'A' && $char <= 'Z');
    }
}

if (!function_exists('isDigit')) {
    function isDigit(?string $char): bool
    {
        return $char >= '0' && $char <= '9';
    }
}
