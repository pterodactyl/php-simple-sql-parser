<?php

require 'vendor/autoload.php';

$path = __DIR__ . DIRECTORY_SEPARATOR . 'test.sql';

$reader = new \PHPSimpleSqlParser\IO\Reader\Reader(file_get_contents($path));
$scanner = new \PHPSimpleSqlParser\Lexer\Lexer($reader);
$parser = new \PHPSimpleSqlParser\Parser($scanner);

$statement = $parser->parse();

var_dump($statement);
